var prepareMailOptions = function (m, d, t, next) {
    var subject = (config.mode != 'production') ? process.env.NODE_ENV + ' * ' : '';
    var options = {
        from: 'Support <' + SUPPORT_ADDRESS + '>',
        subject: subject + t.subject
    };
    if (d.group === 'deliveryBroker') {
        getDeliveries({delivery: m.context.delivery}, function (err, data) {
            if (err || !data) return next(err || new Error('Delivery not found'));
            options.to = (data.broker) ? data.broker.email : TEST_ADDRESS;
            var res = data || {a: 1};
            if (m.context.link) res.link = m.context.link;
            next(null, options, res);
        })
    } else if (!m.receiver) {
        options.to = m.to;
        next(null, options, m.context);
    } else {
        User.findById(m.receiver, function (err, u) {
            if (err || !u) return next(err || new Error('User Not Found'));
            options.to = u.local.email;
            var res = {
                user: u,
                link: m.context.link
            };
            next(null, options, res);
        })
    }
};

var prepareAndroidPushOptions = function (m, d, t, next) {
    var options = {
        delayWhileIdle: true,
        timeToLive: 60
    };
    if (d.group === 'deliveryDriver' && m.receiver) {
        var to = 'User Without Name';
        User.findById(m.receiver, function (err, u) {
            if (err || !u || !u.local.androidTokenId) return next(err || new Error('User Not Found'));
            getDeliveries({delivery: m.context.delivery}, function (err, data) {
                if (err) return next(err);
                to = u.fName + ' ' + u.lName;
                data.open = OPEN;
                data.close = CLOSE;
                var body = ejs.render(t.body, data);
                options.registrationId = u.local.androidTokenId;
                options.data = {
                    alert: body,
                    mId: m._id
                };
                next(null, options, to);
            })
        })
    } else {
        next(new Error('Incorrect push event'));
    }
};

var prepareApplePushOptions = function (m, d, t, next) {
    var options = {};
    if (d.group === 'deliveryDriver' && m.receiver) {
        var to = 'User Without Name';
        User.findById(m.receiver, function (err, u) {
            if (err || !u || !u.local.iosTokenId) return next(err || new Error('User Not Found'));
            getDeliveries({delivery: m.context.delivery}, function (err, data) {
                if (err) return next(err);
                to = u.fName + ' ' + u.lName;
                data.open = OPEN;
                data.close = CLOSE;
                var body = ejs.render(t.body, data);
                options.token = u.local.iosTokenId;
                options.payload = {
                    aps: {
                        alert: body,
                        badge: 1,
                        sound: "default",
                        type: "LSNewDeliveriesPushType",
                        mId: m._id
                    }
                };
                next(null, options, to);
            })
        })
    } else {
        next(new Error('Incorrect push event'));
    }
};

var sendNotification = function (next) {
    NotificationEvents.findOne({status: 'New'}, null, {sort: {date: -1}}, function (err, m) {
        if (err || !m) return next(err);
        NotificationDef
            .findOne({
                type: m.type,
                active: true
            })
            .populate({path: 'template'})
            .exec(function (err, d) {
                if (err) return next(err);
                var t = d.template;
                if (t) {
                    if (m.channel === 'Email') {
                        prepareMailOptions(m, d, t, function (err, o, data) {
                            if (err) return next(err, m);
                            data.open = OPEN;
                            data.close = CLOSE;

                            try {
                                o.html = ejs.render(t.body, data);
                            } catch (e) {
                                m.status = 'Error (Render)';
                                m.save(function (err, message) {
                                    console.log(e);
                                    console.log('\"' + message.type + '\"' + ' mail to ' + o.to + ' ' + message.status.toLowerCase());
                                    return next();
                                });

                                return next();
                            }
                            mailer.sendMail(o, function (error, responce) {
                                if (error) {
                                    console.log(error);
                                    return next(error, m);
                                }
                                m.subject = o.subject;
                                m.to = o.to;
                                m.status = 'Sent';
                                m.body = o.html;
                                m.sentDate = new Date();
                                m.save(function (err, message) {
                                    console.log('\"' + message.type + '\"' + ' mail to ' + o.to + ' ' + message.status.toLowerCase());
                                    next();
                                })
                            });
                        })
                    } else if (m.channel === 'Apple Push') {
                        prepareApplePushOptions(m, d, t, function (err, options, to) {
                            if (err) return next(err, m);
                            m.to = to;
                            m.status = 'Processing';
                            m.body = options.payload.aps.alert;
                            m.save(function (err, message) {
                                if (err) return next(err);

                                // Sending push
                                apn.send(options);

                                return next();
                            })
                        })
                    } else if (m.channel === 'Android Push') {
                        prepareAndroidPushOptions(m, d, t, function (err, options, to) {
                            if (err) return next(err, m);
                            m.to = to;
                            m.status = 'Processing';
                            m.body = options.data.alert;
                            m.save(function (err, message) {
                                if (err) return next(err);
                                console.log(options)
                                // Sending push
                                gcm.send(options);

                                return next();
                            })
                        })
                    }
                } else {
                    next(new Error('Template Not Found'));
                }
            })
    })
};